# -*- coding: utf-8 -*-
from openerp.osv import osv, fields


class mt_document(osv.osv):
    _name = 'mt.document'
    _description = ' Les documents' 
    
    def on_change_report(self, cr, uid, id, report_id, context=None):  
        if(report_id):
            report= self.pool.get('ir.actions.report.xml').browse(cr, uid, report_id, context=context)[0] 
            model=report.model
            return {'value': {'model': model,} }
        return  {}
         
    _columns = {
                'name':fields.char('Nom du document', size=124),
                'report_id': fields.many2one("ir.actions.report.xml", "Nom du Rapport"),
                'structure_ids': fields.one2many("mt.structure","document_id","La stucture de document"), 
                'model' : fields.related('report_id', 'model', type='char', string='Model Name'),  
                'object_mission_id': fields.many2one("mt.object.mission","Catégorie de mission") , 
                'mission_model': fields.many2one("mission.model","Mission modèle"),
                }
    _defaults = {
                 
        }

class mt_stucture(osv.osv):
    _name = 'mt.structure'
    _description = ' Les structures'  
    
    def _get_fields (self, cr, uid, context=None):
        structure_id = context.get('model', False)
        res = dict()
        if(structure_id):
            structure=self.browse(cr, uid, structure_id, context=context)[0]
            table_name=structure.contenu_model_many2one.replace(".","_")
            requete="select column_name from information_schema.columns where table_name = '"+table_name+"'"
            for column in self.cr.fetchall():
                res.insert(column, column)
        return res
         
    _columns = {
                'name':fields.char('Nom de la stucture', size=64),
                'document_id': fields.many2one("mt.document","La stucture de document", ondelete='cascade'),  
                'sequence': fields.integer("sequence"),
                #'type':  fields.selection([('text', 'TEXT'), ('many2one', 'many2one')], 'Type de Structure ', required=False),
                'contenu': fields.html("Le Contenu"), 
                #'contenu_model_many2one': fields.related('document_id', 'model', type='char', string='Model Name', readonly=True), 
                #'fields':fields.selection(_get_fields,  string="Champs"),   
                }
    _defaults = {
                 #'type':'text',
                 
        } 