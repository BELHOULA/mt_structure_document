#  -*- coding: utf-8 -*-

{
    "name": "Structure document",
    "version": "0.1",
    "author": "MT Consulting: RCA",
    "category": 'Tools',
    "description": """
        Ce module permet la création des documents (rapport , lettre ) en html d'une manière dynamique
    """,
    'website': 'http://www.openerp.com',
    'images': [],
    'init_xml': [],
     "depends": ['base','mt_mgmt_audit' ],
    'data': [
             "views/document.xml",
    ],
    'demo_xml': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}
#  vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
